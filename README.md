NDB SESSION CLASS FOR CODEIGNITER

Native Session makes good use of PHP’s native session handling abilities, but it does not allow the use of a database for session storage. 
Saving user session data into database is more secure on any type of hosting (Shared ... ). 
This library overwrites normal native sassion functions too save user data directly into database and gives us some extra functions over CI Session. 

OVERVIEW

Is based on Native Session but has database functionality built in. Compatible with CodeIgniter 1.7 +. Drop-in replacement for CI’s Session library. Config options and flash data are supported but not session encryption. When using with a database, only the session_id is stored in a cookie. Any other data is stored in the database (Nativly would be stored to server /tmp ). Tested IE6, IE7, IE8, IE9, Firefox 4, Chrome. PHP5+ 