-- NDB Session Class MySQL
-- Licence GPL 2.0 and LGPL 2.1
-- Author Denis Molan, Slovenia
-- Copyright 2011 - 2012 by Denis Molan. All Rights Reserved

CREATE  TABLE IF NOT EXISTS `Sessions` (
  `sess_id` VARCHAR(40) NOT NULL ,
  `sess_ip` VARCHAR(16) NOT NULL ,
  `sess_user_agent` VARCHAR(120) NOT NULL ,
  `sess_date_activity` DATETIME NOT NULL ,
  `sess_date_expire` DATETIME NOT NULL ,
  `sess_date_expire_id` DATETIME NOT NULL ,
  `sess_data` TEXT NOT NULL ,
  PRIMARY KEY (`sess_id`) )
ENGINE = InnoDB